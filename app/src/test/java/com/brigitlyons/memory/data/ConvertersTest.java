package com.brigitlyons.memory.data;

import com.brigitlyons.memory.data.implementation.Converters;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ConvertersTest {
    private Date date = new Date();


    @Test
    public void dateToTimestamp() {
        assertEquals((Long) date.getTime(), Converters.dateToTimestamp(date));
    }

    @Test
    public void dateToTimestampWithNullArgument() {
        assertNull(Converters.dateToTimestamp(null));
    }

    @Test
    public void fromTimestamp() {
        assertEquals(date, Converters.fromTimestamp(date.getTime()));
    }

    @Test
    public void fromTimestampWithNullArgument() {
        assertNull(Converters.fromTimestamp(null));
    }
}