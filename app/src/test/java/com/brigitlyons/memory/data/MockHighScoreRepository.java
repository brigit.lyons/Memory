package com.brigitlyons.memory.data;

import android.arch.lifecycle.LiveData;

import java.util.List;

public class MockHighScoreRepository implements HighScoreRepository {

    @Override
    public LiveData<List<HighScore>> getHighestScores(int limit) {
        return null;
    }

    @Override
    public void setHighScore(HighScore highScore) {

    }
}