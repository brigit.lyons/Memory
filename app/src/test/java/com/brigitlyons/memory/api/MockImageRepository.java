package com.brigitlyons.memory.api;

public class MockImageRepository implements ImageRepository {

    @Override
    public void getUrlsForTag(String tag, int size, PhotoUrlsListener listener) {
        listener.handleResponse(new String[]{"URL1"});
    }
}