package com.brigitlyons.memory.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.brigitlyons.memory.api.MockImageRepository;
import com.brigitlyons.memory.data.MockHighScoreRepository;
import com.brigitlyons.memory.domain.Card;
import com.brigitlyons.memory.ui.MemoryCard;
import com.brigitlyons.memory.viewmodels.MemoryGameViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MemoryGameViewModelTest {
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private MemoryGameViewModel viewModel;

    @Before
    public void setUp() {
        viewModel = new MemoryGameViewModel(new MockHighScoreRepository(), new MockImageRepository(), "kitten", 1);
    }

    @Test
    public void initializesTheMemoryCards() {
        List<MemoryCard> memoryCardList = viewModel.getMemoryCardList().getValue();

        assertNotNull(memoryCardList);
        assertEquals(2, memoryCardList.size());
        assertEquals(MemoryCard.create(Card.create(0, Card.State.HIDDEN), "URL1"), memoryCardList.get(0));
        assertEquals(MemoryCard.create(Card.create(0, Card.State.HIDDEN), "URL1"), memoryCardList.get(1));
    }

    @Test
    public void showingCardUpdatesTheMemoryCards() {
        List<MemoryCard> memoryCardList = viewModel.getMemoryCardList().getValue();

        assertEquals(MemoryCard.create(Card.create(0, Card.State.HIDDEN), "URL1"), memoryCardList.get(0));
        assertEquals(MemoryCard.create(Card.create(0, Card.State.HIDDEN), "URL1"), memoryCardList.get(1));

        viewModel.handleCardClick(0);
        memoryCardList = viewModel.getMemoryCardList().getValue();

        assertEquals(MemoryCard.create(Card.create(0, Card.State.SHOWN), "URL1"), memoryCardList.get(0));
        assertEquals(MemoryCard.create(Card.create(0, Card.State.HIDDEN), "URL1"), memoryCardList.get(1));
    }

    @Test
    public void noMatchesWhenOnlyOneCardIsShown() {
        int matches = viewModel.getMatches().getValue();

        assertEquals(0, matches);

        viewModel.handleCardClick(0);
        matches = viewModel.getMatches().getValue();

        assertEquals(0, matches);
    }

    /*
     * The following tests are not working yet.
     *
     * I tried figuring out a way to use a countdown latch to wait for the async task or
     * handler/runnable to finish, but I don't have enough time at the moment to continue looking
     * into it.
     *
     * I think it is a case to be solved in the future ;-)
     */

    //@Test
    public void updatesMatchCountWhenMatchIsMade() {
        int matches = viewModel.getMatches().getValue();

        assertEquals(0, matches);

        viewModel.handleCardClick(0);
        viewModel.handleCardClick(1);
        matches = viewModel.getMatches().getValue();

        assertEquals(1, matches);
    }

    //@Test
    public void updatesMovesCountWhenMatchIsMade() {
        int moves = viewModel.getMoves().getValue();

        assertEquals(0, moves);

        viewModel.handleCardClick(0);
        viewModel.handleCardClick(1);
        moves = viewModel.getMoves().getValue();

        assertEquals(1, moves);
    }

    //@Test
    public void updatesScoreCountWhenMatchIsMade() {
        int score = viewModel.getScore().getValue();

        assertEquals(0, score);

        viewModel.handleCardClick(0);
        viewModel.handleCardClick(1);
        score = viewModel.getScore().getValue();

        assertTrue(0 > score);
    }
}

