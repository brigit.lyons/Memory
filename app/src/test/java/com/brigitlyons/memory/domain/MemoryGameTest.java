package com.brigitlyons.memory.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class MemoryGameTest {
    private MemoryGame memoryGame;

    @Before
    public void setUp() {
        memoryGame = new MemoryGame(newCards());
    }

    @Test
    public void getCardsReturnsCards() {
        assertArrayEquals(newCards(), memoryGame.getCards());

        memoryGame = new MemoryGame(cardsWithIndexOneAndTwoGone());
        assertArrayEquals(cardsWithIndexOneAndTwoGone(), memoryGame.getCards());

        memoryGame = new MemoryGame(cardsWithIndexOneShown());
        assertArrayEquals(cardsWithIndexOneShown(), memoryGame.getCards());
    }

    @Test
    public void showCardShowsCard() {
        memoryGame.showCard(1);

        assertArrayEquals(cardsWithIndexOneShown(), memoryGame.getCards());
    }

    @Test
    public void showCardThatIsAlreadyShownDoesNothing() {
        memoryGame = new MemoryGame(cardsWithIndexOneAndTwoGone());
        memoryGame.showCard(1);

        assertArrayEquals(cardsWithIndexOneAndTwoGone(), memoryGame.getCards());
    }

    @Test
    public void showCardThatIsGoneDoesNothing() {
        memoryGame = new MemoryGame(cardsWithIndexOneShown());
        memoryGame.showCard(1);

        assertArrayEquals(cardsWithIndexOneShown(), memoryGame.getCards());
    }

    @Test
    public void showCardDoesNotIncrementMoves() {
        assertEquals(0, memoryGame.getMoves());

        memoryGame.showCard(1);

        assertEquals(0, memoryGame.getMoves());
    }

    @Test
    public void showCardDoesNotIncrementMatches() {
        assertEquals(0, memoryGame.getMatches());

        memoryGame.showCard(1);

        assertEquals(0, memoryGame.getMatches());
    }

    @Test
    public void successfulMatchIncrementsMatchCount() {
        assertEquals(0, memoryGame.getMatches());

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        memoryGame.showCard(2);
        memoryGame.updateGameState();

        assertEquals(1, memoryGame.getMatches());
    }

    @Test
    public void unsuccessfulMatchDoesNotIncrementMatchCount() {
        assertEquals(0, memoryGame.getMatches());

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        memoryGame.showCard(0);
        memoryGame.updateGameState();

        assertEquals(0, memoryGame.getMatches());
    }

    @Test
    public void showingTheSameCardTwiceMatchDoesIncrementMatchesCount() {
        assertEquals(0, memoryGame.getMatches());

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        assertEquals(0, memoryGame.getMatches());
    }

    @Test
    public void successfulMatchIncrementsMovesCount() {
        assertEquals(0, memoryGame.getMoves());

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        memoryGame.showCard(2);
        memoryGame.updateGameState();

        assertEquals(1, memoryGame.getMoves());
    }

    @Test
    public void unsuccessfulMatchIncrementsMovesCount() {
        assertEquals(0, memoryGame.getMoves());

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        memoryGame.showCard(0);
        memoryGame.updateGameState();

        assertEquals(1, memoryGame.getMoves());
    }

    @Test
    public void showingTheSameCardTwiceMatchDoesIncrementMovesCount() {
        assertEquals(0, memoryGame.getMoves());

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        memoryGame.showCard(1);
        memoryGame.updateGameState();

        assertEquals(0, memoryGame.getMoves());
    }

    @Test
    public void checkForWinOnFinishedBoardReturnsTrue() {
        assertFalse(memoryGame.checkForWin());

        // first match
        memoryGame.showCard(0);
        memoryGame.updateGameState();
        memoryGame.showCard(3);
        memoryGame.updateGameState();

        // second match
        memoryGame.showCard(1);
        memoryGame.updateGameState();
        memoryGame.showCard(2);
        memoryGame.updateGameState();

        assertTrue(memoryGame.checkForWin());
    }

    @Test
    public void checkForWinOnUnfinishedBoardReturnsFalse() {
        memoryGame = new MemoryGame(cardsWithIndexOneAndTwoGone());
        assertFalse(memoryGame.checkForWin());

        // first match
        memoryGame.showCard(0);
        memoryGame.updateGameState();
        memoryGame.showCard(3);
        memoryGame.updateGameState();

        assertFalse(memoryGame.checkForWin());
    }

    @Test
    public void scoreIsZeroUntilMatchIsMade() {
        // first attempt
        memoryGame.showCard(0);
        memoryGame.updateGameState();
        memoryGame.showCard(1);
        memoryGame.updateGameState();

        assertEquals(0, memoryGame.getScore());

        // second attempt
        memoryGame.showCard(1);
        memoryGame.updateGameState();
        memoryGame.showCard(0);
        memoryGame.updateGameState();

        assertEquals(0, memoryGame.getScore());

        // first match
        memoryGame.showCard(0);
        memoryGame.updateGameState();
        memoryGame.showCard(3);
        memoryGame.updateGameState();

        assertNotEquals(0, memoryGame.getScore());
    }

    @Test
    public void scoreIsHigherWhenPerformingTheSameNumberOfMatchesInFewerMoves() {
        // first match
        memoryGame.showCard(0);
        memoryGame.updateGameState();
        memoryGame.showCard(3);
        memoryGame.updateGameState();

        int higherScore = memoryGame.getScore();

        MemoryGame memoryGame2 = new MemoryGame(newCards());

        // first attempt
        memoryGame2.showCard(0);
        memoryGame2.updateGameState();
        memoryGame2.showCard(1);
        memoryGame2.updateGameState();

        // first match
        memoryGame2.showCard(0);
        memoryGame2.updateGameState();
        memoryGame2.showCard(3);
        memoryGame2.updateGameState();

        int lowerScore = memoryGame2.getScore();

        assertTrue(higherScore > lowerScore);
    }

    @Test
    public void scoreIsHigherWhenPerformingTheSameNumberOfMatchesInTheSameNumberOfMovesInGameWithMoreCards() {
        // first match
        memoryGame.showCard(0);
        memoryGame.updateGameState();
        memoryGame.showCard(3);
        memoryGame.updateGameState();

        int lowerScore = memoryGame.getScore();

        MemoryGame memoryGame2 = new MemoryGame(moreCards());

        // first match
        memoryGame2.showCard(0);
        memoryGame2.updateGameState();
        memoryGame2.showCard(3);
        memoryGame2.updateGameState();

        int higherScore = memoryGame2.getScore();

        assertTrue(higherScore > lowerScore);
    }

    @Test
    public void scoreIsZeroWhenNoMovesAreTakenYet() {
        assertEquals(0, memoryGame.getScore());
    }

    private Card[] newCards() {
        return new Card[]{
                Card.create(0, Card.State.HIDDEN),
                Card.create(1, Card.State.HIDDEN),
                Card.create(1, Card.State.HIDDEN),
                Card.create(0, Card.State.HIDDEN)
        };
    }

    private Card[] moreCards() {
        return new Card[]{
                Card.create(0, Card.State.HIDDEN),
                Card.create(1, Card.State.HIDDEN),
                Card.create(1, Card.State.HIDDEN),
                Card.create(0, Card.State.HIDDEN),
                Card.create(2, Card.State.HIDDEN),
                Card.create(2, Card.State.HIDDEN)
        };
    }

    private Card[] cardsWithIndexOneShown() {
        return new Card[]{
                Card.create(0, Card.State.HIDDEN),
                Card.create(1, Card.State.SHOWN),
                Card.create(1, Card.State.HIDDEN),
                Card.create(0, Card.State.HIDDEN)
        };
    }

    private Card[] cardsWithIndexOneAndTwoGone() {
        return new Card[]{
                Card.create(0, Card.State.HIDDEN),
                Card.create(1, Card.State.GONE),
                Card.create(1, Card.State.GONE),
                Card.create(0, Card.State.HIDDEN)
        };
    }
}