package com.brigitlyons.memory.view;

import android.os.Bundle;

public class MemoryGameFragmentParams {
    private final static String EXTRA_IMAGE_TAG = "memory_game_fragment_extra_image_tag";
    private final static String EXTRA_UNIQUE_CARDS = "memory_game_fragment_extra_unique_cards_tag";

    private final static String DEFAULT_IMAGE_TAG = "kitten";
    private final static int DEFAULT_UNIQUE_CARDS = 10;

    private final String imageTag;
    private final int uniqueCards;

    private MemoryGameFragmentParams(String imageTag, int uniqueCards) {
        this.imageTag = imageTag;
        this.uniqueCards = uniqueCards;
    }

    public String getImageTag() {
        return imageTag;
    }

    public int getUniqueCards() {
        return uniqueCards;
    }

    static MemoryGameFragmentParams fromBundle(Bundle bundle) {
        String imageTag =  DEFAULT_IMAGE_TAG;
        int uniqueCards = DEFAULT_UNIQUE_CARDS;
        if (bundle != null) {
            imageTag = bundle.getString(EXTRA_IMAGE_TAG, imageTag);
            uniqueCards = bundle.getInt(EXTRA_UNIQUE_CARDS, uniqueCards);
        }
        return new MemoryGameFragmentParams(imageTag, uniqueCards);
    }

    public static Bundle toBundle(String imageTag, int uniqueCards) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_IMAGE_TAG, imageTag);
        bundle.putInt(EXTRA_UNIQUE_CARDS, uniqueCards);
        return bundle;
    }
}