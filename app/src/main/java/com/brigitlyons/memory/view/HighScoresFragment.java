package com.brigitlyons.memory.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brigitlyons.memory.R;
import com.brigitlyons.memory.adapters.HighScoreListAdapter;
import com.brigitlyons.memory.databinding.HighScoresFragmentBinding;
import com.brigitlyons.memory.util.DependencyInjector;
import com.brigitlyons.memory.viewmodels.HighScoresViewModel;
import com.brigitlyons.memory.viewmodels.HighScoresViewModelFactory;

public class HighScoresFragment extends Fragment {

    private HighScoresViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Obtain the ViewModel component.
        HighScoresViewModelFactory factory = DependencyInjector.provideHighScoresViewModelFactory(requireContext());
        viewModel = ViewModelProviders.of(this, factory).get(HighScoresViewModel.class);

        // Inflate view and obtain an instance of the binding class.
        HighScoresFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.high_scores_fragment, container, false);

        HighScoreListAdapter adapter = new HighScoreListAdapter();
        binding.highScoresList.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.highScoresList.setLayoutManager(layoutManager);

        // Assign the component to a property in the binding class.
        binding.setLifecycleOwner(this);
        subscribeUi(adapter);

        return binding.getRoot();
    }

    private void subscribeUi(HighScoreListAdapter adapter) {
        viewModel.getHighScores().observe(this, highScores -> {
            if (highScores != null) {
                adapter.submitList(highScores);
            }
        });
    }

}
