package com.brigitlyons.memory.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brigitlyons.memory.R;
import com.brigitlyons.memory.adapters.MemoryCardListAdapter;
import com.brigitlyons.memory.databinding.MemoryGameFragmentBinding;
import com.brigitlyons.memory.util.DependencyInjector;
import com.brigitlyons.memory.viewmodels.MemoryGameViewModel;
import com.brigitlyons.memory.viewmodels.MemoryGameViewModelFactory;

public class MemoryGameFragment extends Fragment implements MemoryCardListAdapter.Listener {

    private MemoryGameViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Obtain the ViewModel component.
        MemoryGameViewModelFactory factory = DependencyInjector.provideMemoryGameViewModelFactory(requireContext(), MemoryGameFragmentParams.fromBundle(getArguments()));
        viewModel = ViewModelProviders.of(this, factory).get(MemoryGameViewModel.class);

        // Inflate view and obtain an instance of the binding class.
        MemoryGameFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.memory_game_fragment, container, false);

        MemoryCardListAdapter adapter = new MemoryCardListAdapter(this);
        binding.memoryCardList.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 5);
        binding.memoryCardList.setLayoutManager(layoutManager);

        // Assign the component to a property in the binding class.
        binding.setLifecycleOwner(this);
        binding.setViewmodel(viewModel);
        subscribeUi(adapter);

        return binding.getRoot();
    }

    private void subscribeUi(MemoryCardListAdapter adapter) {
        viewModel.getMemoryCardList().observe(this, memoryCards -> {
            if (memoryCards != null) {
                adapter.submitList(memoryCards);
            }
        });
    }

    @Override
    public void handleCardClick(int position) {
        viewModel.handleCardClick(position);
    }

}
