package com.brigitlyons.memory.domain;

public class MemoryGame {
    private int matches = 0;
    private int moves = 0;
    private Card[] cards;
    private DisplayedCard previousCard = null;
    private DisplayedCard newCard = null;

    public MemoryGame(Card[] cards) {
        this.cards = cards;
    }

    public void showCard(int index) {
        Card.State currentState = cards[index].state();
        switch (currentState) {
            case HIDDEN:
                cards[index] = cards[index].updateState(Card.State.SHOWN);
                newCard = new DisplayedCard(index, cards[index].id());
            default:
                break;
        }
    }

    public boolean isOneCardVisible() {
        return (previousCard == null) && (newCard != null);
    }

    public void updateGameState() {
        if (newCard != null) {
            if (previousCard != null) {
                moves++;
                if (previousCard.id == newCard.id) {
                    handleMatch();
                } else {
                    handleMiss();
                }
            } else {
                previousCard = newCard;
                newCard = null;
            }
        }
    }

    public boolean checkForWin() {
        return matches == cards.length / 2;
    }

    public Card[] getCards() {
        return cards;
    }

    public int getMoves() {
        return moves;
    }

    public int getMatches() {
        return matches;
    }

    public int getScore() {
        if (moves == 0) {
            return 0;
        }
        return (int) (((double) cards.length) / moves) * matches * 75;
    }

    private void handleMatch() {
        cards[previousCard.index] = cards[previousCard.index].updateState(Card.State.GONE);
        cards[newCard.index] = cards[newCard.index].updateState(Card.State.GONE);
        previousCard = null;
        matches++;
    }

    private void handleMiss() {
        cards[previousCard.index] = cards[previousCard.index].updateState(Card.State.HIDDEN);
        cards[newCard.index] = cards[newCard.index].updateState(Card.State.HIDDEN);
        previousCard = null;
        newCard = null;
    }

    class DisplayedCard {
        int index;
        int id;

        DisplayedCard(int index, int id) {
            this.index = index;
            this.id = id;
        }
    }
}
