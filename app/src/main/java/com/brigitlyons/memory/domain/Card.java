package com.brigitlyons.memory.domain;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Card {
    public abstract int id();

    public abstract State state();

    public static Card create(int id, State state) {
        return new AutoValue_Card(id, state);
    }

    Card updateState(State state) {
        return Card.create(id(), state);
    }

    @Override
    public String toString() {
        return "[id: " + id() + " state: " + state().name() + "]";
    }

    public enum State {
        HIDDEN, SHOWN, GONE
    }
}
