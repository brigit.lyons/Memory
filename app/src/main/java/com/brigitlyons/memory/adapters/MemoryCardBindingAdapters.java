package com.brigitlyons.memory.adapters;

import android.databinding.BindingAdapter;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.brigitlyons.memory.ui.MemoryCard;
import com.brigitlyons.memory.util.GlideApp;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import static com.brigitlyons.memory.domain.Card.State.HIDDEN;
import static com.brigitlyons.memory.domain.Card.State.SHOWN;


public class MemoryCardBindingAdapters {

    @BindingAdapter("backVisible")
    public static void backOfCardIsVisible(View view, MemoryCard memoryCard) {
        int visibility = (memoryCard != null && memoryCard.card().state() == HIDDEN) ? View.VISIBLE : View.GONE;
        view.setVisibility(visibility);
    }

    @BindingAdapter("frontVisible")
    public static void frontOfCardIsVisible(View view, MemoryCard memoryCard) {
        int visibility = (memoryCard != null && memoryCard.card().state() == SHOWN) ? View.VISIBLE : View.GONE;
        view.setVisibility(visibility);
    }

    @BindingAdapter("imageFromUrl")
    public static void bindImageFromUrl(ImageView view, @Nullable String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlideApp.with(view.getContext())
                    .load(imageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(view);
        }
    }
}
