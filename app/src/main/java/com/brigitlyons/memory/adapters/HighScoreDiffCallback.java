package com.brigitlyons.memory.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.brigitlyons.memory.data.HighScore;

public class HighScoreDiffCallback extends DiffUtil.ItemCallback<HighScore> {
    @Override
    public boolean areItemsTheSame(@NonNull HighScore oldHighScore, @NonNull HighScore newHighScore) {
        return oldHighScore.getUid() == newHighScore.getUid();
    }

    @Override
    public boolean areContentsTheSame(@NonNull HighScore oldHighScore, @NonNull HighScore newHighScore) {
        return oldHighScore.equals(newHighScore);
    }
}
