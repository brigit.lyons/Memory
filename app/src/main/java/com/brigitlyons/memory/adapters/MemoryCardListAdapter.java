package com.brigitlyons.memory.adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brigitlyons.memory.databinding.MemoryCardBinding;
import com.brigitlyons.memory.ui.MemoryCard;

public class MemoryCardListAdapter extends ListAdapter<MemoryCard, MemoryCardListAdapter.ViewHolder> {
    private Listener listener;

    public MemoryCardListAdapter(Listener listener) {
        super(new MemoryCardDiffCallback());
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        MemoryCard memoryCard = getItem(position);
        viewHolder.bind(memoryCard, createOnClickListener(position), new MemoryCardBindingAdapters());
        viewHolder.itemView.setTag(memoryCard);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        return new ViewHolder(MemoryCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false));
    }

    private View.OnClickListener createOnClickListener(int position) {
        return view -> {
            listener.handleCardClick(position);
            notifyItemChanged(position);
        };
    }

    public interface Listener {
        void handleCardClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        MemoryCardBinding binding;

        ViewHolder(MemoryCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(MemoryCard memoryCard, View.OnClickListener listener, MemoryCardBindingAdapters memoryCardBindingAdapters) {
            binding.setClickListener(listener);
            binding.setMemoryCard(memoryCard);
            binding.setAdapters(memoryCardBindingAdapters);
            binding.executePendingBindings();
        }
    }
}
