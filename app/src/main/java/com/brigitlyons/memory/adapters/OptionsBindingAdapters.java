package com.brigitlyons.memory.adapters;

import android.view.View;

import com.brigitlyons.memory.R;
import com.brigitlyons.memory.view.MemoryGameFragmentParams;

import androidx.navigation.Navigation;

public class OptionsBindingAdapters {
    private static final String IMAGE_TAG = "kitten";

    public static View.OnClickListener easyGameButtonClick() {
        return view -> Navigation.findNavController(view)
                .navigate(R.id.action_optionsFragment_to_memoryGameFragment, MemoryGameFragmentParams.toBundle(IMAGE_TAG, 5));
    }

    public static View.OnClickListener mediumGameButtonClick() {
        return view -> Navigation.findNavController(view)
                .navigate(R.id.action_optionsFragment_to_memoryGameFragment, MemoryGameFragmentParams.toBundle(IMAGE_TAG, 10));
    }

    public static View.OnClickListener hardGameButtonClick() {
        return view -> Navigation.findNavController(view)
                .navigate(R.id.action_optionsFragment_to_memoryGameFragment, MemoryGameFragmentParams.toBundle(IMAGE_TAG, 20));
    }

    public static View.OnClickListener highScoresButtonClick() {
        return view -> Navigation.findNavController(view)
                .navigate(R.id.action_optionsFragment_to_highScoresFragment);
    }
}
