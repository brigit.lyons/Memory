package com.brigitlyons.memory.adapters;

import android.databinding.BindingAdapter;
import android.view.View;

public class BindingAdapters {

    @BindingAdapter("goneUnless")
    public static void goneUnless(View view, Boolean visible) {
        view.setVisibility((visible == null || visible) ? View.VISIBLE : View.GONE);
    }
}
