package com.brigitlyons.memory.adapters;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HighScoreBindingAdapters {

    @BindingAdapter("formatDate")
    public static void formatDate(TextView textView, Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
        textView.setText(format.format(date));
    }
}
