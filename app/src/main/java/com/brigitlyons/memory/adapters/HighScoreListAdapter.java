package com.brigitlyons.memory.adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.brigitlyons.memory.data.HighScore;
import com.brigitlyons.memory.databinding.HighScoreItemBinding;

public class HighScoreListAdapter extends ListAdapter<HighScore, HighScoreListAdapter.ViewHolder> {

    public HighScoreListAdapter() {
        super(new HighScoreDiffCallback());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(HighScoreItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        HighScore highScore = getItem(position);
        viewHolder.bind(highScore, new HighScoreBindingAdapters());
        viewHolder.itemView.setTag(highScore);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        HighScoreItemBinding binding;

        ViewHolder(HighScoreItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(HighScore highScore, HighScoreBindingAdapters highScoreBindingAdapters) {
            binding.setHighScore(highScore);
            binding.setAdapter(highScoreBindingAdapters);
            binding.executePendingBindings();
        }
    }
}
