package com.brigitlyons.memory.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.brigitlyons.memory.ui.MemoryCard;

class MemoryCardDiffCallback extends DiffUtil.ItemCallback<MemoryCard> {

    @Override
    public boolean areItemsTheSame(@NonNull MemoryCard oldMemoryCard, @NonNull MemoryCard newMemoryCard) {
        return oldMemoryCard.card().id() == newMemoryCard.card().id();
    }

    @Override
    public boolean areContentsTheSame(@NonNull MemoryCard oldMemoryCard, @NonNull MemoryCard newMemoryCard) {
        return oldMemoryCard.equals(newMemoryCard);
    }
}
