package com.brigitlyons.memory.data.implementation;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import com.brigitlyons.memory.data.HighScore;

import java.util.concurrent.Executors;

@Database(entities = {HighScore.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract HighScoreDao highScoreDao();

    private static final String DB_NAME = "MemoryGameDatabase.db";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context,
                    AppDatabase.class,
                    DB_NAME)
                    .addCallback(new Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            Executors.newSingleThreadScheduledExecutor().execute(() -> getInstance(context).highScoreDao().insertAll(HighScore.populateData()));
                        }
                    })
                    .build();
        }
        return instance;
    }

    AppDatabase() {
    }
}
