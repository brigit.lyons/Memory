package com.brigitlyons.memory.data;

import android.arch.lifecycle.LiveData;

import java.util.List;

public interface HighScoreRepository {
    LiveData<List<HighScore>> getHighestScores(int limit);

    void setHighScore(HighScore highScore);
}
