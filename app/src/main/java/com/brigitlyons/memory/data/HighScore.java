package com.brigitlyons.memory.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class HighScore {
    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "score")
    private int score;

    @ColumnInfo(name = "date")
    private Date date;

    public HighScore(int score, Date date) {
        this.score = score;
        this.date = date;
    }

    public int getUid() {
        return uid;
    }

    public int getScore() {
        return score;
    }

    public Date getDate() {
        return date;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public static HighScore[] populateData() {
        return new HighScore[]{
                new HighScore(123, new Date()),
                new HighScore(123, new Date()),
                new HighScore(9000, new Date()),
                new HighScore(15, new Date()),
                new HighScore(321, new Date()),
        };
    }
}
