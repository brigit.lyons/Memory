package com.brigitlyons.memory.data.implementation;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.brigitlyons.memory.data.HighScore;

import java.util.List;

@Dao
public interface HighScoreDao {

    @Query("SELECT * FROM highscore ORDER BY score DESC, date DESC LIMIT :limit")
    LiveData<List<HighScore>> getHighestScores(int limit);

    @Insert
    void insertAll(HighScore... highScores);

    @Insert
    void insert(HighScore highScore);

    @Query("DELETE FROM highscore")
    void deleteAll();
}
