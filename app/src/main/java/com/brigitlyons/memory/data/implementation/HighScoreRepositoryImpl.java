package com.brigitlyons.memory.data.implementation;

import android.arch.lifecycle.LiveData;

import com.brigitlyons.memory.data.HighScore;
import com.brigitlyons.memory.data.HighScoreRepository;

import java.util.List;

public class HighScoreRepositoryImpl implements HighScoreRepository {
    private HighScoreDao highScoreDao;

    private HighScoreRepositoryImpl(HighScoreDao highScoreDao) {
        this.highScoreDao = highScoreDao;
    }

    public LiveData<List<HighScore>> getHighestScores(int limit) {
        return highScoreDao.getHighestScores(limit);
    }

    public void setHighScore(HighScore highScore) {
        highScoreDao.insert(highScore);
    }

    private static volatile HighScoreRepositoryImpl instance;

    public static synchronized HighScoreRepositoryImpl getInstance(HighScoreDao highScoreDao) {
        if (instance == null) {
            instance = new HighScoreRepositoryImpl(highScoreDao);
        }
        return instance;
    }
}
