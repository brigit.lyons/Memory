package com.brigitlyons.memory.ui;

import com.brigitlyons.memory.domain.Card;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class MemoryCard {
    public abstract Card card();

    public abstract String imageUrl();

    public static MemoryCard create(int id, String imageUrl) {
        return new AutoValue_MemoryCard(Card.create(id, Card.State.HIDDEN), imageUrl);
    }

    public static MemoryCard create(Card card, String imageUrl) {
        return new AutoValue_MemoryCard(card, imageUrl);
    }
}
