package com.brigitlyons.memory.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MemoryCardDeck {

    public static List<MemoryCard> create(String[] imageUrls) {
        return new ArrayList<>(Arrays.asList(shuffle(initializeCards(imageUrls))));
    }

    private static MemoryCard[] initializeCards(String[] imageUrls) {
        int size = imageUrls.length;
        MemoryCard[] memoryCards = new MemoryCard[size * 2];
        for (int i = 0; i < size; i++) {
            memoryCards[i] = MemoryCard.create(i, imageUrls[i]);
            memoryCards[size + i] = MemoryCard.create(i, imageUrls[i]);
        }
        return memoryCards;

    }

    private static MemoryCard[] shuffle(MemoryCard[] memoryCards) {
        // based on Fisher-Yates shuffle algorithm
        Random random = new Random();
        for (int i = memoryCards.length - 1; i > 0; i--) {
            int randomIndex = random.nextInt(memoryCards.length);
            MemoryCard a = memoryCards[randomIndex];
            memoryCards[randomIndex] = memoryCards[i];
            memoryCards[i] = a;
        }
        return memoryCards;
    }
}
