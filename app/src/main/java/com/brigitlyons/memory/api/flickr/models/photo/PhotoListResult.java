package com.brigitlyons.memory.api.flickr.models.photo;

import com.google.gson.annotations.SerializedName;

public class PhotoListResult {
    @SerializedName("photos")
    private PhotoList photoList;

    public PhotoList getPhotoList() {
        return photoList;
    }
}
