package com.brigitlyons.memory.api.flickr.models.photo;

import com.google.gson.annotations.SerializedName;

public class PhotoList {
    @SerializedName("photo")
    private Photo[] photo;

    public Photo[] getPhoto() {
        return photo;
    }
}
