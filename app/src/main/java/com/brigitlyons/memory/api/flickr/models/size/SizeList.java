package com.brigitlyons.memory.api.flickr.models.size;

import com.google.gson.annotations.SerializedName;

public class SizeList {
    @SerializedName("size")
    private Size[] size;

    public Size[] getSize() {
        return size;
    }
}
