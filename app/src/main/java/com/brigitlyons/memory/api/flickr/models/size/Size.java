package com.brigitlyons.memory.api.flickr.models.size;

import com.google.gson.annotations.SerializedName;

public class Size {
    @SerializedName("label")
    private String label;

    @SerializedName("width")
    private String width;

    @SerializedName("height")
    private String height;

    @SerializedName("source")
    private String source;

    public String getLabel() {
        return label;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getSource() {
        return source;
    }
}
