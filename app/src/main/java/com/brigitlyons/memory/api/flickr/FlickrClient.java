package com.brigitlyons.memory.api.flickr;

import com.brigitlyons.memory.api.flickr.models.photo.PhotoListResult;
import com.brigitlyons.memory.api.flickr.models.size.SizeListResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickrClient {

    String SQUARE_LABEL = "Square";

    @GET("?method=flickr.photos.search&sort=relevence")
    Call<PhotoListResult> getPhotoList(@Query("tags") String tag);

    @GET("?method=flickr.photos.getSizes")
    Call<SizeListResult> getPhotoUrl(@Query("photo_id") String id);
}
