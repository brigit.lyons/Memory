package com.brigitlyons.memory.api;

public interface ImageRepository {

    void getUrlsForTag(String tag, int size, PhotoUrlsListener listener);

    interface PhotoUrlsListener {
        void handleResponse(String[] urls);

        void handleException(Throwable e);
    }
}
