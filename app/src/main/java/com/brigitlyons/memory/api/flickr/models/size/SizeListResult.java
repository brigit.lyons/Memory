package com.brigitlyons.memory.api.flickr.models.size;

import com.google.gson.annotations.SerializedName;

public class SizeListResult {
    @SerializedName("sizes")
    private SizeList sizeList;

    public SizeList getSizeList() {
        return sizeList;
    }
}
