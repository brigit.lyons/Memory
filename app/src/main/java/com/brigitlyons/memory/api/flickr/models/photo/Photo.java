package com.brigitlyons.memory.api.flickr.models.photo;

import com.google.gson.annotations.SerializedName;

public class Photo {
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }
}
