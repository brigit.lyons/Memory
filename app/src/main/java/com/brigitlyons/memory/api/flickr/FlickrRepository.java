package com.brigitlyons.memory.api.flickr;

import android.support.annotation.NonNull;

import com.brigitlyons.memory.api.ImageRepository;
import com.brigitlyons.memory.api.flickr.models.photo.Photo;
import com.brigitlyons.memory.api.flickr.models.photo.PhotoList;
import com.brigitlyons.memory.api.flickr.models.photo.PhotoListResult;
import com.brigitlyons.memory.api.flickr.models.size.Size;
import com.brigitlyons.memory.api.flickr.models.size.SizeList;
import com.brigitlyons.memory.api.flickr.models.size.SizeListResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlickrRepository implements ImageRepository {

    public void getUrlsForTag(String tag, int size, PhotoUrlsListener listener) {
        FlickrClientProvider.getFlickrClient().getPhotoList(tag).enqueue(new Callback<PhotoListResult>() {
            @Override
            public void onResponse(@NonNull Call<PhotoListResult> call, @NonNull Response<PhotoListResult> response) {
                PhotoListResult photoListResult = response.body();
                getSquareUrlsForPhotoListResult(photoListResult, size, listener);
            }

            @Override
            public void onFailure(@NonNull Call<PhotoListResult> call, @NonNull Throwable t) {
                listener.handleException(t);
            }
        });
    }

    private void getSquareUrlsForPhotoListResult(PhotoListResult photoListResult, int size, PhotoUrlsListener listener) {
        if (photoListResult != null) {
            PhotoList photoList = photoListResult.getPhotoList();
            getSquareUrlsForPhotoList(photoList, size, listener);
        } else {
            listener.handleException(new IllegalStateException("PhotoListResult is null"));
        }
    }

    private void getSquareUrlsForPhotoList(PhotoList photoList, int size, PhotoUrlsListener listener) {
        if (photoList != null) {
            Photo[] photos = photoList.getPhoto();
            getSquareUrlsForPhotos(photos, size, listener);
        } else {
            listener.handleException(new IllegalStateException("PhotoList is null"));
        }
    }

    private void getSquareUrlsForPhotos(Photo[] photos, int size, PhotoUrlsListener listener) {
        ArrayList<String> ids = new ArrayList<>();
        if (photos != null) {
            for (int i = 0; i < size; i++) {
                ids.add(photos[i].getId());
            }
            try {
                ArrayList<String> result = getSquareUrlsForPhotoIds(ids, listener);
                listener.handleResponse(result.toArray(new String[result.size()]));
            } catch (Exception e) {
                listener.handleException(e);
            }
        }
    }

    private ArrayList<String> getSquareUrlsForPhotoIds(ArrayList<String> ids, PhotoUrlsListener listener) throws Exception {
        ArrayList<String> results = new ArrayList<>();
        ExecutorService pool = Executors.newFixedThreadPool(ids.size());
        List<Callable<String>> tasks = new ArrayList<>();
        for (String id : ids) {
            tasks.add(() -> {
                Response<SizeListResult> response = FlickrClientProvider.getFlickrClient().getPhotoUrl(id).execute();
                return getSquareSourceFromSizeListResult(response.body(), listener);
            });
        }
        List<Future<String>> futureList = pool.invokeAll(tasks);

        for (Future<String> future : futureList) {
            results.add(future.get());
        }
        return results;
    }

    private String getSquareSourceFromSizeListResult(SizeListResult sizeListResult, PhotoUrlsListener listener) {
        String result = null;
        if (sizeListResult != null) {
            result = getSquareSourceFromSizeList(sizeListResult.getSizeList(), listener);
        } else {
            listener.handleException(new IllegalStateException("SizeListResult is null"));
        }
        return result;
    }

    private String getSquareSourceFromSizeList(SizeList sizeList, PhotoUrlsListener listener) {
        String result = null;
        if (sizeList != null) {
            for (Size size : sizeList.getSize()) {
                if (size != null && size.getLabel().equals(FlickrClient.SQUARE_LABEL)) {
                    result = size.getSource();
                    break;
                }
            }
        } else {
            listener.handleException(new IllegalStateException("SizeList is null"));
        }
        return result;
    }
}
