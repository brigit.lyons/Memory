package com.brigitlyons.memory.api.flickr;

import android.support.annotation.NonNull;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class FlickrClientProvider {
    private static final int MAX_PARALLEL_REQUESTS = 8;
    private static final String BASE_URL = "https://api.flickr.com/services/rest/";
    private static final String API_KEY = "fake";
    private static final long NETWORK_TIMEOUT_SECONDS = 20;

    private static Retrofit retrofit = null;
    private static FlickrClient flickrClient = null;

    static synchronized FlickrClient getFlickrClient() {
        if (flickrClient == null) {
            flickrClient = getRetrofitClient().create(FlickrClient.class);
        }
        return flickrClient;
    }

    private static Retrofit getRetrofitClient() {
        if (retrofit == null) {
            Dispatcher dispatcher = getDispatcher();
            OkHttpClient client = getOkHttpClient(dispatcher);
            retrofit = getRetrofit(client);
        }
        return retrofit;
    }

    @NonNull
    private static Retrofit getRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(client)
                .build();
    }

    @NonNull
    private static Dispatcher getDispatcher() {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(MAX_PARALLEL_REQUESTS);
        return dispatcher;
    }

    @NonNull
    private static OkHttpClient getOkHttpClient(Dispatcher dispatcher) {
        return new OkHttpClient.Builder()
                .dispatcher(dispatcher)
                .connectTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(getInterceptor())
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    @NonNull
    private static Interceptor getInterceptor() {
        return chain -> {
            Request request = chain.request();
            HttpUrl url = request.url().newBuilder()
                    .addQueryParameter("api_key", API_KEY)
                    .addQueryParameter("format", "json")
                    .addQueryParameter("nojsoncallback", "1")
                    .build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        };
    }
}
