package com.brigitlyons.memory.util;

import android.content.Context;

import com.brigitlyons.memory.api.flickr.FlickrRepository;
import com.brigitlyons.memory.data.implementation.AppDatabase;
import com.brigitlyons.memory.data.implementation.HighScoreRepositoryImpl;
import com.brigitlyons.memory.view.MemoryGameFragmentParams;
import com.brigitlyons.memory.viewmodels.HighScoresViewModelFactory;
import com.brigitlyons.memory.viewmodels.MemoryGameViewModelFactory;

public class DependencyInjector {

    private static HighScoreRepositoryImpl getHighScoreRepository(Context context) {
        return HighScoreRepositoryImpl.getInstance(AppDatabase.getInstance(context).highScoreDao());
    }

    public static HighScoresViewModelFactory provideHighScoresViewModelFactory(Context context) {
        HighScoreRepositoryImpl repository = getHighScoreRepository(context);
        return new HighScoresViewModelFactory(repository);
    }

    private static FlickrRepository getFlickrRepository() {
        return new FlickrRepository();
    }

    public static MemoryGameViewModelFactory provideMemoryGameViewModelFactory(Context context, MemoryGameFragmentParams fragmentParams) {
        HighScoreRepositoryImpl repository = getHighScoreRepository(context);
        FlickrRepository flickrRepository = getFlickrRepository();
        return new MemoryGameViewModelFactory(repository, flickrRepository, fragmentParams.getImageTag(), fragmentParams.getUniqueCards());
    }
}
