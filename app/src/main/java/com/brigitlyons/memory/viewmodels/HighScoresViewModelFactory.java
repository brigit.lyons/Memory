package com.brigitlyons.memory.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.brigitlyons.memory.data.implementation.HighScoreRepositoryImpl;

public class HighScoresViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private HighScoreRepositoryImpl highScoreRepository;

    public HighScoresViewModelFactory(HighScoreRepositoryImpl highScoreRepository) {
        this.highScoreRepository = highScoreRepository;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HighScoresViewModel(highScoreRepository);
    }
}
