package com.brigitlyons.memory.viewmodels;

import com.brigitlyons.memory.domain.Card;
import com.brigitlyons.memory.ui.MemoryCard;

import java.util.ArrayList;
import java.util.List;

class MemoryCardMapper {

    static Card[] convertToCardArray(List<MemoryCard> memoryCards) {
        Card[] cards = new Card[memoryCards.size()];
        int i = 0;
        for (MemoryCard memoryCard : memoryCards) {
            cards[i++] = memoryCard.card();
        }
        return cards;
    }

    static List<MemoryCard> updateCardsInMemoryCardList(List<MemoryCard> oldMemoryCards, Card[] cards) {
        List<MemoryCard> newMemoryCards = new ArrayList<>(cards.length);
        int i = 0;
        for (Card card : cards) {
            newMemoryCards.add(MemoryCard.create(card, oldMemoryCards.get(i++).imageUrl()));
        }
        return newMemoryCards;
    }
}
