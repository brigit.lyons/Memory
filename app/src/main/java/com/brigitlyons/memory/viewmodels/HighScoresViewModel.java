package com.brigitlyons.memory.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;

import com.brigitlyons.memory.data.HighScore;
import com.brigitlyons.memory.data.implementation.HighScoreRepositoryImpl;

import java.util.List;

public class HighScoresViewModel extends ViewModel {
    private static final int LIMIT = 10;

    private MediatorLiveData<List<HighScore>> highScoresMerger = new MediatorLiveData<>();

    HighScoresViewModel(HighScoreRepositoryImpl highScoreRepository) {
        LiveData<List<HighScore>> liveHighScores = highScoreRepository.getHighestScores(LIMIT);
        highScoresMerger.addSource(liveHighScores, value -> highScoresMerger.setValue(value));
    }

    public LiveData<List<HighScore>> getHighScores() {
        return highScoresMerger;
    }
}
