package com.brigitlyons.memory.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import com.brigitlyons.memory.api.ImageRepository;
import com.brigitlyons.memory.data.HighScore;
import com.brigitlyons.memory.data.HighScoreRepository;
import com.brigitlyons.memory.domain.MemoryGame;
import com.brigitlyons.memory.ui.MemoryCard;
import com.brigitlyons.memory.ui.MemoryCardDeck;

import java.util.Date;
import java.util.List;

import static com.brigitlyons.memory.viewmodels.MemoryCardMapper.convertToCardArray;
import static com.brigitlyons.memory.viewmodels.MemoryCardMapper.updateCardsInMemoryCardList;

public class MemoryGameViewModel extends ViewModel {
    private HighScoreRepository highScoreRepository;
    private MutableLiveData<Integer> matchesMutable = new MutableLiveData<>();
    private MutableLiveData<Integer> scoreMutable = new MutableLiveData<>();
    private MutableLiveData<Integer> movesMutable = new MutableLiveData<>();
    private MutableLiveData<List<MemoryCard>> memoryCardListMutable = new MutableLiveData<>();
    private MutableLiveData<Boolean> inProgress = new MutableLiveData<>();
    private MutableLiveData<Boolean> error = new MutableLiveData<>();
    private MutableLiveData<Boolean> finished = new MutableLiveData<>();
    private MemoryGame memoryGame;
    private boolean clickable = true;

    public MemoryGameViewModel(HighScoreRepository highScoreRepository, ImageRepository imageRepository, String imageTag, int uniqueCardCount) {
        this.highScoreRepository = highScoreRepository;
        initializeLiveData();
        imageRepository.getUrlsForTag(imageTag, uniqueCardCount, new Listener());
    }

    public LiveData<Integer> getMatches() {
        return matchesMutable;
    }

    public LiveData<Integer> getScore() {
        return scoreMutable;
    }

    public LiveData<Integer> getMoves() {
        return movesMutable;
    }

    public LiveData<List<MemoryCard>> getMemoryCardList() {
        return memoryCardListMutable;
    }

    public LiveData<Boolean> getInProgress() {
        return inProgress;
    }

    public LiveData<Boolean> getError() {
        return error;
    }

    public LiveData<Boolean> getFinished() {
        return finished;
    }

    public void handleCardClick(int position) {
        if (clickable) {
            showCard(position);
            if (memoryGame.isOneCardVisible()) {
                memoryGame.updateGameState();
                return;
            }
            clickable = false;
            new UpdateGameStatusTask().execute();
        }
    }

    private void showCard(int position) {
        memoryGame.showCard(position);
        updateCardsList();
    }

    private void initializeLiveData() {
        matchesMutable.setValue(0);
        scoreMutable.setValue(0);
        movesMutable.setValue(0);
        inProgress.setValue(true);
        error.setValue(false);
        finished.setValue(false);
    }

    private void updateCardsAndStats() {
        memoryGame.updateGameState();

        // update stats
        scoreMutable.setValue(memoryGame.getScore());
        matchesMutable.setValue(memoryGame.getMatches());
        movesMutable.setValue(memoryGame.getMoves());

        updateCardsList();
    }

    private void updateCardsList() {
        memoryCardListMutable.setValue(updateCardsInMemoryCardList(memoryCardListMutable.getValue(), memoryGame.getCards()));
    }

    private void checkForWin() {
        if (memoryGame.checkForWin()) {
            finished.postValue(true);
            highScoreRepository.setHighScore(new HighScore(memoryGame.getScore(), new Date()));
        }
    }

    class Listener implements ImageRepository.PhotoUrlsListener {

        @Override
        public void handleResponse(String[] urls) {
            List<MemoryCard> memoryCardList = MemoryCardDeck.create(urls);
            memoryGame = new MemoryGame(convertToCardArray(memoryCardList));
            inProgress.setValue(false);
            memoryCardListMutable.setValue(memoryCardList);
        }

        @Override
        public void handleException(Throwable e) {
            inProgress.setValue(false);
            error.setValue(true);
        }
    }

    private class UpdateGameStatusTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            clickable = true;
            updateCardsAndStats();
            new CheckForWinTask().execute();
        }
    }

    private class CheckForWinTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            checkForWin();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
