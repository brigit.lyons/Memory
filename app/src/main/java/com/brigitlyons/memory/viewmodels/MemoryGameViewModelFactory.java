package com.brigitlyons.memory.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.brigitlyons.memory.api.ImageRepository;
import com.brigitlyons.memory.data.implementation.HighScoreRepositoryImpl;

public class MemoryGameViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private HighScoreRepositoryImpl highScoreRepository;
    private ImageRepository imageRepository;
    private String imageTag;
    private int uniqueCardCount;

    public MemoryGameViewModelFactory(HighScoreRepositoryImpl highScoreRepository, ImageRepository imageRepository, String imageTag, int uniqueCardCount) {
        this.highScoreRepository = highScoreRepository;
        this.imageRepository = imageRepository;
        this.imageTag = imageTag;
        this.uniqueCardCount = uniqueCardCount;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MemoryGameViewModel(highScoreRepository, imageRepository, imageTag, uniqueCardCount);
    }
}
