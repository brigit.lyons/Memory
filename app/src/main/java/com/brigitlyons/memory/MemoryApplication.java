package com.brigitlyons.memory;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class MemoryApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
