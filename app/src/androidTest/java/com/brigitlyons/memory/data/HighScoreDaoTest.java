package com.brigitlyons.memory.data;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.brigitlyons.memory.data.implementation.AppDatabase;
import com.brigitlyons.memory.data.implementation.HighScoreDao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.List;

import static com.brigitlyons.memory.util.LiveDataTestUtil.getValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class HighScoreDaoTest {
    private HighScoreDao highScoreDao;

    private HighScore highScore1 = new HighScore(1234, new Date(1));
    private HighScore highScore2 = new HighScore(123, new Date(3));
    private HighScore highScore3 = new HighScore(123, new Date(2));
    private HighScore highScore4 = new HighScore(4321, new Date(2));

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        AppDatabase database = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        highScoreDao = database.highScoreDao();

        // Insert high scores in random order to ensure the results are sorted by highest to lowest score
        highScoreDao.insertAll(highScore3, highScore2, highScore1);
    }

    @Test
    public void getHighestScores() throws InterruptedException {
        List<HighScore> highScores = getValue(highScoreDao.getHighestScores(3));
        assertEquals(3, highScores.size());

        assertTrue(compareHighScore(highScore1, highScores.get(0)));
        assertTrue(compareHighScore(highScore2, highScores.get(1)));
        assertTrue(compareHighScore(highScore3, highScores.get(2)));
    }

    @Test
    public void getHighestScoresLimitsTheNumberOfResults() throws InterruptedException {
        List<HighScore> highScores = getValue(highScoreDao.getHighestScores(2));
        assertEquals(2, highScores.size());

        assertTrue(compareHighScore(highScore1, highScores.get(0)));
        assertTrue(compareHighScore(highScore2, highScores.get(1)));
    }

    @Test
    public void getHighestScoresReturnsLessThanLimitIfNecessary() throws InterruptedException {
        List<HighScore> highScores = getValue(highScoreDao.getHighestScores(5));
        assertEquals(3, highScores.size());

        assertTrue(compareHighScore(highScore1, highScores.get(0)));
        assertTrue(compareHighScore(highScore2, highScores.get(1)));
        assertTrue(compareHighScore(highScore3, highScores.get(2)));
    }

    @Test
    public void canInsertAndRetrieveNewHighScore() throws InterruptedException {
        List<HighScore> highScores = getValue(highScoreDao.getHighestScores(5));
        assertEquals(3, highScores.size());

        assertTrue(compareHighScore(highScore1, highScores.get(0)));
        assertTrue(compareHighScore(highScore2, highScores.get(1)));
        assertTrue(compareHighScore(highScore3, highScores.get(2)));

        highScoreDao.insert(highScore4);

        assertEquals(3, highScores.size());
        highScores = getValue(highScoreDao.getHighestScores(5));

        assertTrue(compareHighScore(highScore4, highScores.get(0)));
        assertTrue(compareHighScore(highScore1, highScores.get(1)));
        assertTrue(compareHighScore(highScore2, highScores.get(2)));
    }

    @Test
    public void canDeleteAllHighScores() throws InterruptedException {
        List<HighScore> highScores = getValue(highScoreDao.getHighestScores(5));
        assertEquals(3, highScores.size());

        highScoreDao.deleteAll();
        highScores = getValue(highScoreDao.getHighestScores(5));

        assertEquals(0, highScores.size());
    }

    private boolean compareHighScore(HighScore highScore, @Nullable Object obj) {
        return obj instanceof HighScore &&
                highScore.getScore() == ((HighScore) obj).getScore() &&
                highScore.getDate().compareTo(((HighScore) obj).getDate()) == 0;
    }
}