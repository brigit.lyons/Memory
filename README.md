# Memory

This an Android application for the game of [Memory](https://en.wikipedia.org/wiki/Concentration_(game)).

This app is written in Java, avoids using RxJava, and uses clean architecture.

The card images are sourced from Flickr and can be modified by changing the image tag used for searching. This tag is currently hardcoded to `kitten`. If you build from this source, you will first need to add your Flickr API key [here](https://gitlab.com/brigit.lyons/Memory/blob/master/app/src/main/java/com/brigitlyons/memory/api/flickr/FlickrClientProvider.java#L21).

Using this API requires making n + 1 network calls when you need to request n image urls, so some multithreading has been used (max 8 threads).

Several of the Android Architecture components are used:

* [Room](https://developer.android.com/topic/libraries/architecture/room)
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [Data Binding](https://developer.android.com/topic/libraries/data-binding/)
* [ViewModels](https://developer.android.com/topic/libraries/architecture/viewmodel)
* [Navigation](https://developer.android.com/topic/libraries/architecture/navigation/)

Some other libraries have also been used:

* [Stetho](https://github.com/facebook/stetho)
* [Retrofit](https://square.github.io/retrofit/)
* [OkHttp3](https://square.github.io/okhttp/3.x/okhttp/) (with Stetho and Retrofit)
* [Glide](https://github.com/bumptech/glide)
* [gson](https://github.com/google/gson)

The testing approach is to avoid reliance on mocking libraries, such as [Mockito](https://github.com/mockito/mockito), in favor of using dependency injection with interfaces that have separate app and test implementations. It is preferred to write unit tests over instrumentation tests, since they do not require an Android environment to run on and are faster. Not everything is tested perfectly, but the most complex components have been tested to give an example of best practice, e.g.:

* MemoryGameTest
* MemoryGameViewModelTest
* HighScoreDaoTest

Areas for future improvement:

* Adding more tests
* Replace AsyncTask with another solution (RxJava might be nice ;-) )
* Error handling when loading individual card images
* Fallback images for cards when network requests fail
* Pre-fetching of image urls/images (WorkManager might be nice)
* Caching of Flickr API requests
* Changing the action bar headers to match the fragment names
* Adding up arrow support in the action bar
* Empty/error state for high scores screen
* Remove prepopulated date from high scores screen
* Ability to clear High Score history
* Ability to start a new game from the Game screen
* Allow user to specify the image tag for sourcing images
* More intelligent use of the Flickr API. Currently, some of the images you get in the response may not be super related to the image tag used for searching
* General UI improvements
* Card flip animations